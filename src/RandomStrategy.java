import java.util.Random;

/**
 * Created by amen on 8/21/17.
 */
public class RandomStrategy implements IInputStrategy {

    private Random random = new Random();

    public RandomStrategy() {
    }

    @Override
    public String getString() {
        return random.toString();
    }

    @Override
    public double getDouble() {
    return random.nextDouble();
    }

    @Override
    public int getInt() {
    return random.nextInt();
    }
}
